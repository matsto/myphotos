module models

@[table: 'gallery']
pub struct Gallery {
pub mut:
	id         int     @[primary; sql: serial]
	name       string  @[required; unique]
	created_at string  @[default: 'CURRENT_TIMESTAMP']
	updated_at string
	deleted_at string
	active     bool    @[default: true]
	photos     []Photo @[fkey: 'gallery_id']
}

@[table: 'gallery_user_linkage']
pub struct GalleryLinkage {
pub mut:
	id         int @[primary; sql: serial]
	gallery_id int @[references: 'gallery'; required; unique: 'link']
	user_id    int @[references: 'users'; required; unique: 'link']
}

@[table: 'fav_photo_user_linkage']
pub struct FavPhotoLinkage {
pub mut:
	id              int @[primary; sql: serial]
	photo_id        int @[references: 'photos'; required; unique: 'plink']
	gallery_link_id int @[references: 'gallery_user_linkage'; required; unique: 'plink']
}

@[table: 'photos']
pub struct Photo {
pub mut:
	id         int    @[primary; sql: serial]
	path       string @[required; unique]
	order      int    @[required; sql: 'ord'; unique: 'gal']
	gallery_id int    @[references: 'gallery'; required; unique: 'gal']
	etag       string @[required; unique]
}
