module services

import db.sqlite
import crypto.bcrypt
import models

pub fn add_user(db sqlite.DB, name string, email string, password string, admin bool) !models.User {
	hashed_password := bcrypt.generate_from_password(password.bytes(), bcrypt.min_cost) or {
		return err
	}

	user_model := models.User{
		name: name
		email: email
		password: hashed_password
		active: true
		admin: admin
	}

	sql db {
		insert user_model into models.User
	} or { return err }

	users := sql db {
		select from models.User where name == name limit 1
	}!

	return users.first()
}

fn get_user_by_id(db sqlite.DB, user_id int) !models.User {
	users := sql db {
		select from models.User where id == user_id
	}!

	return users.first()
}

pub fn get_all_user(db sqlite.DB) ![]models.User {
	results := sql db {
		select from models.User
	}!

	return results
}

pub fn get_by_name(db sqlite.DB, name string) !models.User {
	users := sql db {
		select from models.User where email == name limit 1
	}!

	if users.len == 0 {
		return error('user not found')
	}

	user := users.first()
	if !user.active {
		return error('user is not active')
	}
	return user
}

pub fn delete_user(db sqlite.DB, name string) ! {
	sql db {
		delete from models.User where name == name
	}!
}
