module services

import models
import db.sqlite
import os
import crypto.md5
import net.http

pub fn get_gallery(db sqlite.DB, id string, user_id string) !models.Gallery {
	rows := db.exec_param_many('SELECT
		g.id,
		g.name,
		g.active
		FROM gallery_user_linkage l
		JOIN gallery g
		ON l.gallery_id = g.id
		WHERE l.user_id = ($1) AND g.id = ($2)
		LIMIT 1',
		[user_id, id])!
	if rows.len == 0 {
		return error('Gallery not found')
	}
	if rows.len > 1 {
		return error('Too many galleries returned')
	}
	r := rows[0]
	gal := models.Gallery{
		id: r.vals[0].int()
		name: r.vals[1].str()
		active: r.vals[2].bool()
	}
	return gal
}

pub fn get_user_galleries(db sqlite.DB, user_id string) ![]models.Gallery {
	mut galleries := []models.Gallery{}

	rows := db.exec_param('SELECT
	g.id,
	g.name,
	g.active
	FROM gallery_user_linkage l
	JOIN gallery g
	ON l.gallery_id = g.id
	WHERE l.user_id = ($1)',
		user_id)!
	for r in rows {
		galleries << models.Gallery{
			id: r.vals[0].int()
			name: r.vals[1].str()
			active: r.vals[2].bool()
		}
	}
	return galleries
}

pub fn get_photos_by_gallery_id(db sqlite.DB, gallery_id string, user_id string, start_id string, limit string) ![]models.Photo {
	mut photos := []models.Photo{}
	mut query := 'SELECT
	p.id, p.path, p.ord, p.gallery_id, p.etag
	FROM photos p
	JOIN gallery_user_linkage g
	ON p.gallery_id = g.gallery_id
	WHERE g.user_id = ? AND g.gallery_id = ? AND p.ord > ?
	ORDER BY p.ord
	LIMIT ?'
	mut params := [user_id, gallery_id, start_id, limit]

	if start_id == '' && limit == '' {
		query = 'SELECT
		p.id, p.path, p.ord, p.gallery_id, p.etag
		FROM photos p
		JOIN gallery_user_linkage g
		ON p.gallery_id = g.gallery_id
		WHERE g.user_id = ? AND g.gallery_id = ?
		ORDER BY p.ord'
		params = [user_id, gallery_id]
	}
	rows := db.exec_param_many(query, params)!

	for r in rows {
		photos << models.Photo{
			id: r.vals[0].int()
			path: r.vals[1]
			order: r.vals[2].int()
			gallery_id: r.vals[3].int()
			etag: r.vals[4]
		}
	}
	return photos
}

pub fn delete_photos_by_gallery_id(db sqlite.DB, gallery_id string) ! {
	photos := sql db {
		select from models.Photo where gallery_id == gallery_id
	}!
	for photo in photos {
		os.rm('public/${gallery_id}/${photo.path}') or { return error('Could not delete file') }
	}

	sql db {
		delete from models.Photo where gallery_id == gallery_id
	}!
}

pub fn get_photo_by_id(db sqlite.DB, photo_id string, gallery_id string, user_id string) !models.Photo {
	rows := db.exec_param_many('SELECT
	p.id, p.path, p.ord, p.gallery_id, p.etag
	FROM photos p
	JOIN gallery_user_linkage g
	ON p.gallery_id = g.gallery_id
	WHERE p.id = ? AND g.user_id = ? AND g.gallery_id = ?
	LIMIT 1',
		[photo_id, user_id, gallery_id])!

	if rows.len == 0 {
		return error('Photo not found')
	}
	if rows.len > 1 {
		return error('Too many photos returned')
	}

	r := rows[0]

	return models.Photo{
		id: r.vals[0].int()
		path: r.vals[1]
		order: r.vals[2].int()
		gallery_id: r.vals[3].int()
		etag: r.vals[4]
	}
}

pub fn get_liked_photos(db sqlite.DB, gallery_id string, user_id string) ![]models.FavPhotoLinkage {
	fav_rows := db.exec_param_many('SELECT f.id, f.photo_id, f.gallery_link_id
		FROM fav_photo_user_linkage f
		JOIN gallery_user_linkage g ON g.id = f.gallery_link_id
		WHERE g.user_id = ? AND g.gallery_id = ?',
		[user_id, gallery_id])!
	mut fav_photos := []models.FavPhotoLinkage{}
	for f in fav_rows {
		fav_photos << models.FavPhotoLinkage{
			id: f.vals[0].int()
			photo_id: f.vals[1].int()
			gallery_link_id: f.vals[2].int()
		}
	}
	return fav_photos
}

pub fn like_photo(db sqlite.DB, photo_id string, gallery_id string, user_id string) !bool {
	fav_rows := db.exec_param_many('SELECT * FROM fav_photo_user_linkage f
		JOIN gallery_user_linkage g ON g.id = f.gallery_link_id
		WHERE g.user_id = ? AND g.gallery_id = ? AND f.photo_id = ?
		LIMIT 1',
		[user_id, gallery_id, photo_id])!
	gal_linkages := sql db {
		select from models.GalleryLinkage where gallery_id == gallery_id && user_id == user_id limit 1
	}!

	if gal_linkages.len == 0 {
		return error('Gallery not found')
	}
	gal_linkage := gal_linkages.first()

	if fav_rows.len == 0 {
		new_fav := models.FavPhotoLinkage{
			gallery_link_id: gal_linkage.id
			photo_id: photo_id.int()
		}

		sql db {
			insert new_fav into models.FavPhotoLinkage
		}!
		return true
	}
	sql db {
		delete from models.FavPhotoLinkage where photo_id == photo_id
		&& gallery_link_id == gal_linkage.id
	}!
	return false
}

pub fn add_photo_to_gallery(db sqlite.DB, file http.FileData, gallery_id string, order string) !models.Photo {
	os.mkdir_all('public/${gallery_id}/') or { return error('Could not create folder') }

	mut f := os.create('public/${gallery_id}/${file.filename}') or {
		return error('Could not create file')
	}

	defer {
		f.close()
	}
	f.write(file.data.bytes()) or { return error('Could not write to the file') }
	etag := md5.hexhash(file.data)

	photo := models.Photo{
		path: file.filename
		order: order.int()
		gallery_id: gallery_id.int()
		etag: etag
	}
	sql db {
		insert photo into models.Photo
	} or {
		delete_photo_file(file.filename, gallery_id)!
		return err
	}
	photos := sql db {
		select from models.Photo where path == photo.path && etag == photo.etag limit 1
	} or { return error('Could not select photo') }
	return photos.first()
}

pub fn delete_photo_file(filename string, gallery_id string) ! {
	os.rm('public/${gallery_id}/${filename}')!
}

pub fn delete_photo(db sqlite.DB, photo_id string) ! {
	photos := sql db {
		select from models.Photo where id == photo_id.int() limit 1
	}!
	photo := photos.first()

	sql db {
		delete from models.Photo where id == photo_id.int()
	}!
	delete_photo_file(photo.path, photo.gallery_id.str())!
}

pub fn add_gallery(db sqlite.DB, name string) !models.Gallery {
	gallery := models.Gallery{
		name: name
		active: true
	}
	sql db {
		insert gallery into models.Gallery
	}!
	galleries := sql db {
		select from models.Gallery where name == gallery.name limit 1
	}!
	return galleries.first()
}

pub fn delete_gallery(db sqlite.DB, gallery_id string) ! {
	sql db {
		delete from models.GalleryLinkage where gallery_id == gallery_id
		delete from models.Gallery where id == gallery_id.int()
	}!
}

pub fn create_linkage(db sqlite.DB, linkage models.GalleryLinkage) !models.GalleryLinkage {
	sql db {
		insert linkage into models.GalleryLinkage
	} or { return error('Could not create linkage') }

	linkages := sql db {
		select from models.GalleryLinkage where gallery_id == linkage.gallery_id
		&& user_id == linkage.user_id limit 1
	}!
	if linkages.len == 0 {
		return error('Could not find linkage')
	}
	if linkages.len > 1 {
		return error('Too many linkages returned')
	}
	return linkages.first()
}

pub fn delete_linkage(db sqlite.DB, gallery_id string, user_id string) ! {
	sql db {
		delete from models.GalleryLinkage where gallery_id == gallery_id && user_id == user_id
	}!
}
