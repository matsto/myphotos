module main

import rand
import vweb
import time
import net.http
import os

const port = 8180
const ip = '127.0.0.1'
const base_url = 'http://${ip}:${port}'

fn testsuite_begin() {
	db := setup_database(':memory:') or {
		assert err.msg() == ''
		return
	}
	logger := setup_logger('${os.temp_dir()}/${rand.uuid_v4()}') or {
		assert err.msg() == ''
		return
	}

	app := new_app(db, logger)
	spawn vweb.run(app, port)
	time.sleep(1 * time.second)
}

fn testsuite_end() {
	// add cleanup here
}

fn test_ping() {
	resp := http.get(base_url + '/ping/') or {
		assert err.msg() == ''
		return
	}
	assert resp.body == 'ping', resp.body
}
