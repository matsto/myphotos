module dtos

pub struct PhotoSchema {
pub:
	id         int    @[required]
	order      int    @[required]
	gallery_id int    @[required]
	etag       string @[required]
}

pub struct GallerySchema {
pub:
	id         int    @[required]
	name       string @[required]
	created_at string @[required]
	active     bool   @[required]
}

pub struct UserLinkage {
pub:
	user_id int @[required]
}
