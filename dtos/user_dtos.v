module dtos

pub struct UserJWTData {
pub:
	id    string @[json: 'sub']
	name  string @[json: 'name']
	iat   int    @[json: 'iat']
	exp   int    @[json: 'exp']
	admin bool   @[json: 'admin']
}

pub struct UserOutSchema {
pub:
	id         int    @[required]
	name       string @[required]
	email      string @[required]
	created_at string @[required]
	updated_at string @[required]
	active     bool   @[required]
}
