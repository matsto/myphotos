module main

import models
import db.sqlite

fn create_db_connection(path string) !sqlite.DB {
	return sqlite.connect(path)!
}

fn setup_database(path string) !sqlite.DB {
	mut db := create_db_connection(path)!

	db.exec('pragma foreign_keys = ON')!
	db.exec('pragma journal_mode = WAL')!

	sql db {
		create table models.User
		create table models.Gallery
		create table models.Photo
		create table models.FavPhotoLinkage
		create table models.GalleryLinkage
	}!

	return db
}
