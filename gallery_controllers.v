module main

import vweb
import models
import services
import os
import dtos
import json

@['/galleries/'; get]
pub fn (mut app App) page_galleries() vweb.Result {
	galleries := services.get_user_galleries(app.db, app.user.id) or { []models.Gallery{} }
	return $vweb.html()
}

@['/galleries/:id/view/:photo_id'; get]
pub fn (mut app App) page_carousel(id string, photo_id string) vweb.Result {
	gallery := services.get_gallery(app.db, id, app.user.id) or {
		// TODO: change to not allowed
		println(err)
		println('could not get photo')
		return app.not_found()
	}

	photos := services.get_photos_by_gallery_id(app.db, id, app.user.id, '', '') or {
		[]models.Photo{}
	}
	if photos.len == 0 {
		println('photos not found')
		return app.not_found()
	}
	mut photo := photos[0]
	mut prev_photo := photos[photos.len - 1]
	mut next_photo := photos[0]
	for idx, p in photos {
		if p.id == photo_id.int() {
			if idx != 0 {
				photo = p
				prev_photo = photos[idx - 1]
			}
			if idx != photos.len - 1 {
				next_photo = photos[idx + 1]
			}
			break
		}
	}

	back_url := '/galleries/${id}'
	mut liked_photo_ids := []int{}
	for p in services.get_liked_photos(app.db, id, app.user.id) or { []models.FavPhotoLinkage{} } {
		liked_photo_ids << p.photo_id
	}
	return $vweb.html()
}

@['/galleries/:id'; get]
pub fn (mut app App) page_sidebar(id string) vweb.Result {
	gallery := services.get_gallery(app.db, id, app.user.id) or {
		// TODO: change to not allowed
		println(err)
		println('could not get photo')
		return app.not_found()
	}
	offset := app.query['offset'] or { '0' }

	photos := services.get_photos_by_gallery_id(app.db, id, app.user.id, offset, '12') or {
		[]models.Photo{}
	}
	if photos.len == 0 {
		println('photos not found')
		return app.not_found()
	}
	mut liked_photo_ids := []int{}
	for photo in services.get_liked_photos(app.db, id, app.user.id) or {
		[]models.FavPhotoLinkage{}
	} {
		liked_photo_ids << photo.photo_id
	}
	return $vweb.html()
}

@['/galleries/:id/photos/:photo_id/like'; post]
pub fn (mut app App) photo_like(id string, photo_id string) vweb.Result {
	services.get_photo_by_id(app.db, photo_id, id, app.user.id) or {
		// TODO: change to not allowed
		return app.not_found()
	}

	liked := services.like_photo(app.db, photo_id, id, app.user.id) or { return app.not_found() }
	if liked {
		return app.html('<i id="newPicCol${photo_id}" class="bi bi-heart-fill fs-2 text-danger"></i>')
	}
	return app.html('<i id="newPicCol${photo_id}" class="bi bi-heart fs-2 text-danger"></i>')
}

@['/galleries/:id/photos/:photo_id'; get]
pub fn (mut app App) photo_view(id string, photo_id string) vweb.Result {
	photo := services.get_photo_by_id(app.db, photo_id, id, app.user.id) or {
		// TODO: change to not allowed
		println(err)
		println('could not get photo')
		return app.not_found()
	}
	app.add_header('Cache-Control', 'max-age=31536000')
	app.add_header('ETag', photo.etag)
	return app.file(os.join_path(os.resource_abs_path('.'), 'public', id, photo.path))
}

// API

@['/api/galleries/'; get]
pub fn (mut app App) list_galleries() vweb.Result {
	galleries := services.get_user_galleries(app.db, app.user.id) or { []models.Gallery{} }
	return app.json(galleries)
}

@['/api/galleries/'; post]
pub fn (mut app App) add_gallery() vweb.Result {
	mut body := json.decode(models.Gallery, app.req.data) or {
		app.set_status(400, '')
		return app.text('Failed to decode json, error: ${err}')
	}
	body = services.add_gallery(app.db, body.name) or {
		app.set_status(400, '')
		return app.text('Failed to decode json, error: ${err}')
	}
	gallery := dtos.GallerySchema{
		id: body.id
		name: body.name
		created_at: body.created_at
		active: body.active
	}

	return app.json(gallery)
}

@['/api/galleries/:id'; get]
pub fn (mut app App) get_gallery(id string) vweb.Result {
	gallery := services.get_gallery(app.db, id, app.user.id) or {
		// access denied
		return app.not_found()
	}
	return app.json(gallery)
}

@['/api/galleries/:id'; delete]
pub fn (mut app App) delete_gallery(id string) vweb.Result {
	f := app.query['force'] or { 'false' }
	if f == 'true' {
		services.delete_photos_by_gallery_id(app.db, id) or { return app.not_found() }
	}
	services.delete_gallery(app.db, id) or {
		// not found
		return app.not_found()
	}
	return app.ok('')
}

@['/api/galleries/:id/photos/'; post]
pub fn (mut app App) add_photo(id string) vweb.Result {
	files := app.files['photo']
	if files.len == 0 {
		println('no files')
		return app.not_found()
	}
	if files.len > 1 {
		println('too many files')
		return app.not_found()
	}
	file := files[0]
	if file.data == '' {
		println('no data')
		return app.not_found()
	}
	if file.filename.ends_with('.webp') == false {
		println('not a webp')
		return app.not_found()
	}
	order := app.form['order']
	if order == '' {
		println('no order')
		return app.not_found()
	}

	photo := services.add_photo_to_gallery(app.db, file, id, order) or {
		println(err)
		return app.not_found()
	}
	p := dtos.PhotoSchema{
		id: photo.id
		order: photo.order
		gallery_id: photo.gallery_id
		etag: photo.etag
	}

	return app.json(p)
}

@['/api/galleries/:id/users/'; post]
pub fn (mut app App) link_user_to_gallery(id string) vweb.Result {
	l := json.decode(dtos.UserLinkage, app.req.data) or {
		app.set_status(400, '')
		return app.text('Failed to decode json, error: ${err}')
	}
	mut link := models.GalleryLinkage{
		user_id: l.user_id
		gallery_id: id.int()
	}
	link = services.create_linkage(app.db, link) or {
		app.set_status(400, '')
		return app.text('Failed to save to db, error: ${err}')
	}
	return app.json(link)
}

@['/api/galleries/:id/users/:user_id'; delete]
pub fn (mut app App) delete_gallery_user_link(id string, user_id string) vweb.Result {
	services.delete_linkage(app.db, id, user_id) or {
		app.set_status(400, '')
		return app.text('Failed to delete linkage, error: ${err}')
	}
	return app.ok('')
}

@['/api/galleries/:id/liked_photos/:user_id'; get]
pub fn (mut app App) list_liked_photos(id string, user_id string) vweb.Result {
	liked_photos := services.get_liked_photos(app.db, id, user_id) or {
		app.set_status(500, '')
		return app.text('Failed to retrieve liked photos, error: ${err}')
	}
	return app.json(liked_photos)
}
