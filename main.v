module main

import vweb
import db.sqlite
import log
import dtos
import os
import flag
import services

const http_port = 8088

struct App {
	vweb.Context
	middlewares map[string][]vweb.Middleware
pub mut:
	db     sqlite.DB
	logger log.Log          @[vweb_global]
	user   dtos.UserJWTData
}

pub fn (mut app App) before_request() {
	log_request_middleware(mut app)
	auth_middleware(mut app)
}

enum Mode as u8 {
	init
	run
}

struct Args {
	mode           Mode
	admin_name     string
	admin_password string
}

fn new_args(mode string, name string) !Args {
	m := match mode {
		'init' {
			Mode.init
		}
		'run' {
			Mode.run
		}
		else {
			return error('invalid mode')
		}
	}
	if m == .init && name == '' {
		return error('admin name cannot be empty')
	}

	password := match m {
		.init { os.input('Enter your password: ') }
		else { '' }
	}

	if m == .init && password == '' {
		return error('password cannot be empty')
	}
	if m == .run && password != '' {
		return error('password cannot be provided in run mode')
	}
	return Args{
		mode: m
		admin_name: mode
		admin_password: password
	}
}

fn parse_args() !Args {
	mut fp := flag.new_flag_parser(os.args)
	fp.application('flag_example_tool')
	fp.version('v0.0.1')
	fp.limit_free_args(0, 0)! // comment this, if you expect arbitrary texts after the options
	fp.description('This tool is only designed to show how the flag lib is working')
	fp.skip_executable()
	mode := fp.string('mode', `m`, 'run', 'mode of operation')
	admin_name := fp.string('admin_name', `n`, '', 'provide an admin name')
	return new_args(mode, admin_name)
}

fn main() {
	args := parse_args() or {
		eprintln('Error: ${err}')
		exit(1)
	}

	mut db := setup_database('database.db')!
	defer {
		db.close() or {
			eprintln('DB closing failed')
			panic(err)
		}
	}
	logger := setup_logger('./logs')!
	if args.mode == .run {
		mut app := new_app(db, logger)

		app.mount_static_folder_at(os.join_path(os.resource_abs_path('.'), 'templates',
			'js'), '/static/js')
		app.mount_static_folder_at(os.join_path(os.resource_abs_path('.'), 'templates',
			'css'), '/static/css')
		app.serve_static('/favicon.ico', os.join_path(os.resource_abs_path('.'), 'assets',
			'favicon.ico'))
		app.serve_static('/logo.png', os.join_path(os.resource_abs_path('.'), 'assets',
			'logo.png'))
		vweb.run(app, http_port)
	} else if args.mode == .init {
		services.add_user(db, args.admin_name, args.admin_password, args.admin_name, true) or {
			eprintln('Error: ${err}')
			exit(1)
		}
		println('Admin user created successfully')
	}
}

fn new_app(db &sqlite.DB, logger &log.Log) &App {
	mut app := &App{
		db: db
		logger: logger
		middlewares: {
			// chaining is allowed, middleware will be evaluated in order
			'/':    [authorize_middleware, refresh_token_middleware]
			'/api': [admin_middleware]
		}
	}

	return app
}

@['/'; get]
pub fn (mut app App) page_home() vweb.Result {
	return app.redirect('/login')
}

@['/ping/'; get]
pub fn (mut app App) ping() ?vweb.Result {
	return app.text('ping')
}

pub fn (mut app App) not_found() vweb.Result {
	app.set_status(404, 'Not Found')
	return app.html('<h1>Page not found</h1>')
}
