# myPhotos
Version: 0.1.0 (beta)

## Overall goal
Reason to create this app is to help photographers with their communication with clients. If your clients send you lists of file names of photos they have chosen to be printed look no further this app has it covered.

An app creates very simple web gallery which allows clients to chose photos they like. Using instagram-like flow which should be familiar to everyone. Selected photos then can be retrieved by the client.

Thanks to this approach your business looks more professional than sending jpg's and names back-and-forth. Also limits clients' invention, who got screenshots of chosen photos instead of their names knows what I am talking about here. 

## Architecture
This project is server side of the whole system.

There is also a client app written in GO [here](https://gitlab.com/matsto/myphotosuploader).

It helps with loading data to the server, creating users, galleries and retrieving data.


## Installation
This project does not provide any binary builds so it requires source compilation.

### Prerequisites
To get it working Vlang compiler is required, please  follow these steps: https://blog.vlang.io/getting-started-with-v/

### Installation process
1. go to the root folder of the project
1. type `v .`, this command should compile project to a binary which appears in the root folder named `myPhotos`
1. this binary can be either run locally or deployed to the server

### Help requests
If you need help with deploying this app, please, create an issue in this repo.

## Roadmap
1. secure against CSRF
1. add tests
1. add missing endpoints
