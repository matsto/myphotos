module main

import json
import dtos
import services
import regex
import net.http
import models
import vweb

fn log_request_middleware(mut app App) {
	app.info('[Vweb] ${app.Context.req.method} ${app.Context.req.url}')
}

const safe_paths = {
	'GET':  [
		'/',
		'/ping/',
		'/login/',
	]
	'POST': [
		'/login/',
	]
}

const static_file_paths = {
	'GET': [
		'/static/css/*',
		'/static/js/*',
	]
}

fn auth_middleware(mut app App) bool {
	if auth_jwt_header(mut app) {
		return true
	}
	if auth_jwt_cookie(mut app) {
		return true
	}

	return false
}

fn auth_jwt_cookie(mut app App) bool {
	cookie := app.get_cookie('token') or { return false }

	return auth_jwt(mut app, cookie)
}

fn auth_jwt_header(mut app App) bool {
	auth_header := app.get_header('Authorization')
	if auth_header == '' {
		return false
	}

	token := auth_header.all_after('Bearer ').trim(' ')
	return auth_jwt(mut app, token)
}

fn auth_jwt(mut app App, token string) bool {
	authed, payload := services.auth_verify(token)
	if !authed {
		return false
	}
	user := json.decode(dtos.UserJWTData, payload) or {
		app.error(err.str())
		app.set_status(500, '')
		app.text('HTTP 500: Internal Error')
		return false
	}
	app.user = user
	app.set_value('user', user)
	return true
}

fn refresh_token_middleware(mut app vweb.Context) bool {
	if app.req.method == .post && app.req.url == '/api/auth/login/' {
		return true
	}
	content_type := app.get_header('Content-Type')
	if content_type == 'application/json' {
		return true
	}
	u := app.get_value[dtos.UserJWTData]('user') or { dtos.UserJWTData{} }
	if u.id == '' {
		return true
	}
	valid := (u.exp - u.iat) / 60

	user := models.User{
		id: u.id.int()
		name: u.name
		password: ''
		email: ''
	}

	token := services.make_token(user, valid)

	app.set_cookie(http.Cookie{
		name: 'token'
		value: token
		path: '/'
		secure: true
		http_only: true
		max_age: valid * 60
	})
	return true
}

fn admin_middleware(mut app vweb.Context) bool {
	if app.req.method == .post && app.req.url == '/api/auth/login/' {
		return true
	}
	user := app.get_value[dtos.UserJWTData]('user') or { dtos.UserJWTData{} }
	if !user.admin {
		app.set_status(403, 'Forbidden')
		app.json('HTTP 403: Forbidden')
		return false
	}
	return true
}

fn authorize_middleware(mut app vweb.Context) bool {
	if app.req.method == .post && app.req.url == '/api/auth/login/' {
		return true
	}
	if app.req.method == .options {
		return true
	}
	if app.req.url.to_lower() in safe_paths[app.req.method.str().to_upper()] {
		return true
	}
	templates := static_file_paths[app.req.method.str().to_upper()]
	for template in templates {
		query := '^' + template.replace('*', r'\S+') + '$'
		mut re := regex.regex_opt(query) or { continue }
		start, end := re.match_string(app.req.url.to_lower())
		if start == 0 && end == app.req.url.len_utf8() {
			return true
		}
	}
	user := app.get_value[dtos.UserJWTData]('user') or { dtos.UserJWTData{} }
	if user.id == '' {
		app.redirect('/login/')

		return false
	}
	return true
}
