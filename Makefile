tests:
	v test .

fmt:
	v fmt -w .

run:
	v watch run . -g $(args)

vet:
	v vet .
